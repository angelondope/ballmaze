﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour
{
    public float _SmoothFactor;
    private Rigidbody mRigidbody;
    private Vector3 mDirection;

    private void Awake()
    {
        mRigidbody = GetComponent<Rigidbody>();
        mDirection = Vector3.zero;
    }

    private void FixedUpdate()
    {
        mDirection.x = Input.acceleration.x;
        mDirection.y = 0;
        mDirection.z = Input.acceleration.y;
        mRigidbody.AddForce(mDirection * Time.fixedDeltaTime * _SmoothFactor, ForceMode.Impulse);
    }
}
