﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputModes : MonoBehaviour
{

    public bool _IsKeyboard;
    public float _KeySmoothFactor;

    void FixedUpdate ()
    {
        if (_IsKeyboard)
        {
            transform.Rotate(0, 0, Time.fixedDeltaTime * Input.GetAxis("Horizontal") * _KeySmoothFactor);
            transform.Rotate(Time.fixedDeltaTime * Input.GetAxis("Vertical") * _KeySmoothFactor, 0, 0);
        }
    }
}
